package dexter.example.com.location;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;

import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;
import com.rm.freedrawview.FreeDrawSerializableState;
import com.rm.freedrawview.FreeDrawView;

import java.util.Random;

public class PaintActivity extends AppCompatActivity {


    FreeDrawView freeDrawView;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint);

        freeDrawView = findViewById(R.id.freeDrawView);
        fab = findViewById(R.id.floatingActionButton);

        SubActionButton.Builder itemBuilder = new SubActionButton.Builder(this);

        // Need to pass the location details when the activity is called
        // from the original activity to restore the state stored
        // initially if there is any

        // freeDrawView.restoreStateFromSerializable(passedState);
        ImageView pencilIcon = new ImageView(this);
        pencilIcon.setImageResource(R.drawable.pencil);

        ImageView clearIcon = new ImageView(this);
        clearIcon.setImageResource(R.drawable.clear);

        ImageView saveIcon = new ImageView(this);
        saveIcon.setImageResource(R.drawable.save);

        SubActionButton clearButton = itemBuilder.setContentView(clearIcon).build();
        SubActionButton storeButton = itemBuilder.setContentView(saveIcon).build();
        SubActionButton colorButton = itemBuilder.setContentView(pencilIcon).build();


        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freeDrawView.clearDraw();
            }
        });

        colorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int returnColor[] = {Color.BLACK,Color.GREEN,Color.BLUE,Color.RED,Color.YELLOW};
                int index = (int) (Math.random() * returnColor.length);
                freeDrawView.setPaintColor(returnColor[index]);

            }
        });

        storeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FreeDrawSerializableState state = freeDrawView.getCurrentViewStateAsSerializable();

                // need to save this state in the db along with details of location
            }
        });

        FloatingActionMenu menu = new FloatingActionMenu.Builder(this)
                .addSubActionView(clearButton)
                .addSubActionView(storeButton)
                .addSubActionView(colorButton)
                .setStartAngle(-30)
                .setEndAngle(-150)
                .attachTo(fab)
                .build();

    }
}
